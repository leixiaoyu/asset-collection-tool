﻿using System.Net;
using AssetCollection.ConsoleApp.Contracts;
using RestSharp;

namespace AssetCollection.ConsoleApp
{
    internal class AssetCollectionClient
    {
        private readonly RestClient _client;
        private const string SERVICE_BASE_URL = "https://assetcollection.documents.cimpress.io/";
        private const string REFRESH_TOKEN = "2SRhPToIEuOm5bSm0eMDbYEpicPHhmO9rx44VGZN3acQo";
        
        private static string ACCESS_TOKEN = string.Empty;

        public AssetCollectionClient()
        {
            _client = new RestClient(SERVICE_BASE_URL);
        }

        public bool TryInsertIntoAssetCollection(bool regenerateToken, Collection collection)
        {
            if (regenerateToken || string.IsNullOrEmpty(ACCESS_TOKEN))
            {
                ACCESS_TOKEN = GetJwtFromAuth0();
            }

            var request = new RestRequest("/v1/collections", Method.POST);
            request.AddHeader("Authorization", "Bearer " + ACCESS_TOKEN);

            request.AddJsonBody(collection);
            var response = _client.Execute(request);

            return response.StatusCode == HttpStatusCode.Created;
        }

        private string GetJwtFromAuth0()
        {
            var client = new RestClient("https://cimpress.auth0.com/");
            var request = new RestRequest("delegation", Method.POST);

            var body = new Auth0Request
            {
                client_id = "QkxOvNz4fWRFT6vcq79ylcIuolFz2cwN",
                grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer",
                refresh_token = REFRESH_TOKEN,
                target = "S3c2UjyfdFlzbZlhavOkgnav1QFkKkfA",
                scope = "openid name email connection"
            };

            request.AddJsonBody(body);
            
            var response = client.Execute<Auth0Response>(request);
            return response.Data.id_token;
        }
    }
}
