﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetCollection.ConsoleApp.Contracts
{
    /// <summary>
    /// Asset
    /// </summary>
    public class Asset
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Last modified time
        /// </summary>
        public DateTime? Modified { get; set; }

        /// <summary>
        /// Decoration technology
        /// </summary>
        public string DecorationTechnology { get; set; }

        /// <summary>
        /// Size is in MM
        /// </summary>
        public double WidthInMm { get; set; }

        /// <summary>
        /// Height in MM
        /// </summary>
        public double HeightInMm { get; set; }

        /// <summary>
        /// Preview URL
        /// </summary>
        public string PreviewUrl { get; set; }

        /// <summary>
        /// Asset URL
        /// </summary>
        public string AssetUrl { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public List<Metadatum> Metadata { get; set; }

        /// <summary>
        /// Asset categories
        /// </summary>
        public List<AssetCategory> Categories { get; set; }

        /// <summary>
        /// Asset keywords
        /// </summary>
        public List<Keyword> Keywords { get; set; }

        public void AddUpdateMetadataElement(Metadatum element)
        {
            if (Metadata == null)
            {
                Metadata = new List<Metadatum>();
            }

            var existingElement = Metadata.SingleOrDefault(x => x.Id == element.Id);
            if (existingElement == null)
            {
                Metadata.Add(element);
            }
            else
            {
                existingElement.Value = element.Value;
            }
        }

        public void AddCategory(AssetCategory category)
        {
            if (Categories == null)
            {
                Categories  = new List<AssetCategory>();
            }

            var existingCategory =
                Categories.SingleOrDefault(x => x.Path == category.Path && x.Culture == category.Culture);
            if (existingCategory == null)
            {
                category.Id = category.Id ?? Guid.NewGuid().ToString();
                Categories.Add(category);
            }
        }

        public void AddKeyword(Keyword keyword)
        {
            if (Keywords == null)
            {
                Keywords = new List<Keyword>();
            }

            var existingKeyword = Keywords.SingleOrDefault(x => x.Culture == keyword.Culture);
            if (existingKeyword == null)
            {
                Keywords.Add(new Keyword(keyword.Culture, keyword.Values));
            }
            else
            {
                existingKeyword.Values = existingKeyword.Values.Union(keyword.Values).ToList();
            }
        }
    }
}
