﻿using System;

namespace AssetCollection.ConsoleApp.Contracts
{
    /// <summary>
    /// Asset category
    /// </summary>
    public class AssetCategory
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Culture
        /// </summary>
        public string Culture { get; set; }

        public AssetCategory() { }

        public AssetCategory(string path, string culture)
        {
            Id = Guid.NewGuid().ToString();
            Path = path;
            Culture = culture;
        }
    }
}
