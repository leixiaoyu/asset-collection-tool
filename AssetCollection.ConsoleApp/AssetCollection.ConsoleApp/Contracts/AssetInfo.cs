﻿namespace AssetCollection.ConsoleApp.Contracts
{
    internal class AssetInfo
    {
        public string FileName { get; set; }

        public string PreviewFileName { get; set; }

        public string Category { get; set; }

        public string CategoryCulture { get; set; }

        public string Keyword { get; set; }

        public string KeywordCulture { get; set; }

        public string MetadataKey { get; set; }

        public string MetadataValue { get; set; }

        public string Collection { get; set; }

        public string DecorationTechnology { get; set; }

        public double WidthInMm { get; set; }

        public double HeightInMm { get; set; }
    }
}
