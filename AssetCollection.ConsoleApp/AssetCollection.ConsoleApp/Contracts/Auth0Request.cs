﻿namespace AssetCollection.ConsoleApp.Contracts
{
    public class Auth0Request
    {
        public string client_id { get; set; }

        public string grant_type { get; set; }

        public string refresh_token { get; set; }

        public string target { get; set; }

        public string scope { get; set; }
    }
}
