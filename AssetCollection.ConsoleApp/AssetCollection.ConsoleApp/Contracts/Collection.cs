﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetCollection.ConsoleApp.Contracts
{
    /// <summary>
    /// Asset collection
    /// </summary>
    public class Collection
    {
        private const string NAME_METADATA_KEY = "name";
        
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Last modified time
        /// </summary>
        public DateTime? Modified { get; set; }

        /// <summary>
        /// Assets
        /// </summary>
        public List<Asset> Assets { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public List<Metadatum> MetaData { get; set; }


        #region Public Functions

        /// <summary>
        /// Set name of the collection
        /// </summary>
        /// <param name="name"></param>
        public void SetName(string name)
        {
            if (MetaData == null)
            {
                MetaData = new List<Metadatum>();
            }

            var nameElement = MetaData.SingleOrDefault(x => x.Id == NAME_METADATA_KEY);

            if (nameElement == null)
            {
                MetaData.Add(new Metadatum {Id = NAME_METADATA_KEY, Value = name});
            }
            else
            {
                nameElement.Value = name;
            }
        }

        /// <summary>
        /// Get collection name
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            if (MetaData == null || !MetaData.Any())
            {
                return null;
            }

            var nameElement = MetaData.SingleOrDefault(x => x.Id == NAME_METADATA_KEY);
            if (nameElement == null)
            {
                return null;
            }

            return nameElement.Value;
        }

        /// <summary>
        /// Add metadata element. If metadata exists, update its value
        /// </summary>
        /// <param name="element"></param>
        public void AddUpdateMetaDataElement(Metadatum element)
        {
            if (MetaData == null)
            {
                MetaData = new List<Metadatum>();
            }

            var existingElement = MetaData.SingleOrDefault(x => x.Id == element.Id);
            if (existingElement == null)
            {
                MetaData.Add(element);
            }
            else
            {
                existingElement.Value = element.Value;
            }
        }

        public void AddAsset(Asset asset)
        {
            if (Assets == null)
            {
                Assets = new List<Asset>();
            }

            Assets.Add(asset);
        }

        #endregion
    }
}
