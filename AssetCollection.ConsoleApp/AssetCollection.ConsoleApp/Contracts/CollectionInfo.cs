﻿namespace AssetCollection.ConsoleApp.Contracts
{
    public class CollectionInfo
    {
        public string Name { get; set; }

        public string MetadataKey { get; set; }

        public string MetadataValue { get; set; }
    }
}
