﻿using System.Collections.Generic;

namespace AssetCollection.ConsoleApp.Contracts
{
    public class Group
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Merchant
        /// </summary>
        public string Merchant { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public List<Metadatum> MetaData { get; set; }

        /// <summary>
        /// Collection Id
        /// </summary>
        public List<string> CollectionIds { get; set; }
    }
}
