﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AssetCollection.ConsoleApp.Contracts
{
    /// <summary>
    /// Keyword
    /// </summary>
    public class Keyword
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Culture
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// Values
        /// </summary>
        public List<string> Values { get; set; }

        public Keyword() { }

        public Keyword(string culture, IEnumerable<string> values)
        {
            Id = Guid.NewGuid().ToString();
            Culture = culture;
            Values = values.ToList();
        }
    }
}
