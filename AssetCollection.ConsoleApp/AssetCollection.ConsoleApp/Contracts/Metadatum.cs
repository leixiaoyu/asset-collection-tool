﻿namespace AssetCollection.ConsoleApp.Contracts
{
    public class Metadatum
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }

        public Metadatum() { }

        public Metadatum(string id, string value)
        {
            Id = id;
            Value = value;
        }
    }
}
