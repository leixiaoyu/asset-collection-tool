﻿using System;

namespace AssetCollection.ConsoleApp.Contracts
{
    internal class UploadResponse
    {
        public bool analysisIsLogo { get; set; }
        public bool analysisIsPhoto { get; set; }
        public double analysisLineartness { get; set; }
        public string availableVariants { get; set; }
        public int deleteAfterDays { get; set; }
        public DateTime lastModifiedTime { get; set; }
        public string originalFileContentType { get; set; }
        public int originalFileSize { get; set; }
        public int previewFileContentLength { get; set; }
        public string previewFileContentType { get; set; }
        public int printFileContentLength { get; set; }
        public string printFileContentType { get; set; }
        public int printPixelHeight { get; set; }
        public int printPixelWidth { get; set; }
        public bool processingComplete { get; set; }
        public string processingEngine { get; set; }
        public string processingGuidStamp { get; set; }
        public DateTime processingTimeEnd { get; set; }
        public DateTime processingTimeStart { get; set; }
        public string processingType { get; set; }
        public string tenant { get; set; }
        public int thumbFileContentLength { get; set; }
        public string thumbFileContentType { get; set; }
        public string uploadedFileName { get; set; }
        public string uploadId { get; set; }
        public string uploadIdNoCluster { get; set; }

    }
}
