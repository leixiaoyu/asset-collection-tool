﻿using System.Collections.Generic;
using AssetCollection.ConsoleApp.Contracts;

namespace AssetCollection.ConsoleApp.Dtos
{
    internal class AssetDto
    {
        public string FileName { get; set; }

        public string PreviewUrl { get; set; }

        public string AssetUrl { get; set; }

        public string CollectionName { get; set; }

        public IList<AssetCategory> Categories { get; set; }

        public IList<Keyword> Keywords { get; set; }

        public IList<Metadatum> Metadata { get; set; }

    }
}
