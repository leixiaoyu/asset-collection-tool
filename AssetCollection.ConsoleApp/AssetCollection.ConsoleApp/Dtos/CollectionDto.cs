﻿using System;
using System.Collections.Generic;
using AssetCollection.ConsoleApp.Contracts;

namespace AssetCollection.ConsoleApp.Dtos
{
    internal class CollectionDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Last modified time
        /// </summary>
        public DateTime? Modified { get; set; }

        /// <summary>
        /// Assets
        /// </summary>
        public List<Asset> Assets { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        public List<Metadatum> MetaData { get; set; }


    }
}
