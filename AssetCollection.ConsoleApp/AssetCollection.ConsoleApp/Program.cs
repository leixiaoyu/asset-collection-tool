﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using AssetCollection.ConsoleApp.Contracts;
using AssetCollection.ConsoleApp.Services;
using CsvHelper;
using Newtonsoft.Json;

namespace AssetCollection.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Expects a CSV file and a folder of images awaiting for upload
             * CSV file contains image file full name, keywords, categories, and groups information
             * 
             */

            /*
             * Register dependencies below
             */

            List<Asset> assets;
            List<Collection> collections;
            var collectionClient = new AssetCollectionClient();
            var uploadClient = new UploadClient();
            var collectionService = new CollectionService(collectionClient);
            var assetService = new AssetService(uploadClient);

            /*
             * Register dependencies above
             */

            Console.WriteLine(" => Hello there.");

            // GET TENANT NAME
            var tenant = GetUserInput("What's the tenant name in upload storage for these assets?");

            // GET RESOURCE PATHES
            var basePath = GetUserInput("Where is the root folder?");
            var collectionFilePath = Path.Combine(basePath, "collections.csv");
            var assetFilePath = Path.Combine(basePath, "assets.csv");
            var assetFolderPath = Path.Combine(basePath, "assets");
            Console.WriteLine(" => ");
            Console.WriteLine(" => Here are pathes of all files needed, inferred from base path.");
            Console.WriteLine(" => Make sure all files and folders are named correctly.");
            Console.WriteLine(" => ");
            Console.WriteLine(" => COLLECTION FILE    :: " + collectionFilePath);
            Console.WriteLine(" => ASSET FILE         :: " + assetFilePath);
            Console.WriteLine(" => IMAGE/ASSET FOLDER :: " + assetFolderPath);
            Console.WriteLine(" => TENANT NAME        :: " + tenant);

            Console.WriteLine(" => ");
            Console.WriteLine(" => About to import data...");
            Console.WriteLine(" => ");

            // READ IN COLLECTION DATA
            using (var streamReader = new StreamReader(collectionFilePath, Encoding.UTF8))
            {
                var reader = new CsvReader(streamReader);
                var collectionRecords = reader.GetRecords<CollectionInfo>();
                collections = collectionService.ConvertCollectionInfoToCollectionContract(collectionRecords).ToList();
            }

            Console.WriteLine(" => COLLECTION DATA ACQUIRED");

            // READ IN ASSET DATA
            using (var streamReader = new StreamReader(assetFilePath, Encoding.UTF8))
            {
                var reader = new CsvReader(streamReader);
                var assetRecords = reader.GetRecords<AssetInfo>();
                assets = assetService.ConvertAssetInfoToAssetContract(assetFolderPath, tenant, assetRecords).ToList();
            }

            Console.WriteLine(" => ASSET DATA ACQUIRED");

            // COMBINE COLLECTION AND ASSET DATA
            collections = collectionService.InsertAssetsIntoCollections(assets, collections).ToList();

            // PREVIEW
            var preview = GetUserConfirmation("Do you want to preview imported data?");
            if (preview)
            {
                var json = JsonConvert.SerializeObject(collections, Formatting.Indented);
                Console.WriteLine(json);

                // Save formatted JSON to disk
                using (var streamWriter = new StreamWriter(Path.Combine(basePath, "asset-collection.json")))
                {
                    streamWriter.Write(json);
                    streamWriter.WriteLine();
                }

                Console.WriteLine(" => Press any key to continue...");
                Console.ReadKey();
            }

            // UPLOAD CONTENTS
            foreach (var collection in collections)
            {
                Console.WriteLine(" => About to publish collection " + collection.Id);
                var success = collectionService.PublishCollectionToService(collection);
                Console.WriteLine(" => Publish collection " + collection.Id + " success? " + success);
            }
            
            Console.WriteLine(" => The end. Press any key to exit...");
            Console.ReadKey();
        }

        private static string GetUserInput(string question)
        {
            while (true)
            {
                Console.WriteLine(" => ");
                Console.WriteLine(" => " + question);
                var answer = Console.ReadLine();

                Console.WriteLine("[YOU SAY]: \n => " + answer);
                Console.WriteLine(" => Is this correct?");
                Console.WriteLine(" => Press C to re-enter value. Press any other key to continue...");
                if (Console.ReadKey().Key != ConsoleKey.C)
                {
                    Console.WriteLine(" => ");
                    return answer;
                }
            }
        }

        private static bool GetUserConfirmation(string phrase)
        {
            while (true)
            {
                Console.WriteLine(" => ");
                Console.WriteLine(" => " + phrase);
                Console.WriteLine(" => Press Y for Yes. Press any other key for No.");
                var answer = Console.ReadKey().Key == ConsoleKey.Y;

                Console.WriteLine("[YOU SAY]: \n => " + (answer ? "Yes" : "No"));
                Console.WriteLine(" => Are you sure?");
                Console.WriteLine(" => Press C to re-enter value. Press any other key to continue...");
                if (Console.ReadKey().Key != ConsoleKey.C)
                {
                    Console.WriteLine(" => ");
                    return answer;
                }
            }
        }
    }
}
