﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AssetCollection.ConsoleApp.Contracts;

namespace AssetCollection.ConsoleApp.Services
{
    internal class AssetService
    {
        private const string COLLECTION_METADATA_KEY = "collection";
        private readonly UploadClient _client;

        public AssetService(UploadClient client)
        {
            _client = client;
        }

        public IEnumerable<Asset> ConvertAssetInfoToAssetContract(string basePath, string tenantName, IEnumerable<AssetInfo> assetInfos)
        {
            var current = new AssetInfo();
            var currentId = string.Empty;
            var assets = new List<Asset>();
            foreach (var info in assetInfos)
            {
                // If it is for a new asset, create a new asset instance
                if (info.FileName != current.FileName)
                {
                    current = info;

                    // Upload files to upload storage
                    Task<string> previewTask = null;
                    var uploadTask = UploadAssetToUploadStorage(tenantName, Path.Combine(basePath, info.FileName));

                    if (!string.IsNullOrEmpty(info.PreviewFileName))
                    {
                        previewTask = UploadAssetToUploadStorage(tenantName,
                            Path.Combine(basePath, info.PreviewFileName));
                    }

                    var asset = new Asset
                    {
                        Id = Guid.NewGuid().ToString(),
                        DecorationTechnology = info.DecorationTechnology,
                        HeightInMm = info.HeightInMm,
                        WidthInMm = info.WidthInMm,
                        Modified = DateTime.UtcNow,
                        Categories = new List<AssetCategory>
                        {
                            new AssetCategory(info.Category, info.CategoryCulture)
                        },
                        Keywords = new List<Keyword>
                        {
                            new Keyword(info.KeywordCulture, new List<string> {info.Keyword})
                        }
                    };

                    asset.AddUpdateMetadataElement(new Metadatum(info.MetadataKey, info.MetadataValue));

                    // Add collection information as a metadata element
                    asset.AddUpdateMetadataElement(new Metadatum(COLLECTION_METADATA_KEY, info.Collection));

                    asset.AssetUrl = uploadTask.Result;
                    if (previewTask != null)
                    {
                        asset.PreviewUrl = previewTask.Result;
                    }
                    else
                    {
                        asset.PreviewUrl = asset.AssetUrl + "/preview";
                    }

                    currentId = asset.Id;
                    assets.Add(asset);
                }
                else
                {
                    var workingAsset = assets.Single(x => x.Id == currentId);
                    workingAsset.AddUpdateMetadataElement(new Metadatum(info.MetadataKey, info.MetadataValue));
                    workingAsset.AddCategory(new AssetCategory(info.Category, info.CategoryCulture));
                    workingAsset.AddKeyword(new Keyword(info.KeywordCulture, new List<string> {info.Keyword}));
                }
            }

            return assets;
        }

        private Task<string> UploadAssetToUploadStorage(string tenantName, string assetPath)
        {
            return Task.Run(() => _client.UploadAnAssetToStorage(false, tenantName, assetPath));
        }
    }
}
