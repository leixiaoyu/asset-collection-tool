﻿using System;
using System.Collections.Generic;
using System.Linq;
using AssetCollection.ConsoleApp.Contracts;

namespace AssetCollection.ConsoleApp.Services
{
    internal class CollectionService
    {
        private const string COLLECTION_METADATA_KEY = "collection";
        private readonly AssetCollectionClient _client;

        public CollectionService(AssetCollectionClient client)
        {
            _client = client;
        }
        
        /// <summary>
        /// Convert collection from a file to collection objects
        /// </summary>
        /// <param name="collectionInfos"></param>
        /// <returns></returns>
        public IEnumerable<Collection> ConvertCollectionInfoToCollectionContract(
            IEnumerable<CollectionInfo> collectionInfos)
        {
            var current = string.Empty;
            var collections = new List<Collection>();

            foreach (var info in collectionInfos)
            {
                if (info.Name != current)
                {
                    current = info.Name;
                    var collection = new Collection
                    {
                        Id = Guid.NewGuid().ToString(),
                        Modified = DateTime.UtcNow
                    };
                    collection.SetName(info.Name);
                    collection.AddUpdateMetaDataElement(new Metadatum
                    {
                        Id = info.MetadataKey,
                        Value = info.MetadataValue
                    });
                    collections.Add(collection);
                }
                else
                {
                    var collection = collections.Single(x => x.GetName() == info.Name);
                    collection.AddUpdateMetaDataElement(new Metadatum
                    {
                        Id = info.MetadataKey,
                        Value = info.MetadataValue
                    });
                }
            }

            return collections;
        }
        
        /// <summary>
        /// Insert assets into collections
        /// </summary>
        /// <param name="assets"></param>
        /// <param name="collections"></param>
        /// <returns></returns>
        public IEnumerable<Collection> InsertAssetsIntoCollections(List<Asset> assets, List<Collection> collections)
        {
            foreach (var asset in assets)
            {
                var collectionInfo = asset.Metadata.SingleOrDefault(x => x.Id == COLLECTION_METADATA_KEY);
                if (collectionInfo == null)
                {
                    throw new InvalidOperationException("Asset has not specified collection name.");
                }

                var collection = collections.SingleOrDefault(x => x.GetName() == collectionInfo.Value);
                if (collection == null)
                {
                    throw new InvalidOperationException("Collection not found with name " + collectionInfo.Value);
                }

                collection.AddAsset(asset);
            }

            return collections;
        }

        /// <summary>
        /// publish collection to asset collection service
        /// </summary>
        /// <param name="collection"></param>
        public bool PublishCollectionToService(Collection collection)
        {
            var success = _client.TryInsertIntoAssetCollection(false, collection);

            if (!success)
            {
                success = _client.TryInsertIntoAssetCollection(true, collection);
            }

            return success;
        }
    }
}
