﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using AssetCollection.ConsoleApp.Contracts;
using RestSharp;

namespace AssetCollection.ConsoleApp
{
    internal class UploadClient
    {
        private const string UPLOAD_BASE_URL = "https://uploads.documents.cimpress.io/";
        private readonly RestClient _client;

        public UploadClient()
        {
            _client = new RestClient(UPLOAD_BASE_URL);
        }

        public string UploadAnAssetToStorage(bool storeOriginal, string tenantName, string assetPath)
        {
            var request = new RestRequest("/v1/uploads", Method.POST);

            var query = storeOriginal
                ? "{\"type\":\"image\", \"arguments\":{\"storeOriginal\":\"true\"}"
                : "{\"type\":\"image\"}";
            request.AddQueryParameter("process", query);
            request.AddQueryParameter("tenant", tenantName);

            var fileName = Path.GetFileName(assetPath);
            request.AddFile(fileName, assetPath);

            var response = _client.Execute<List<UploadResponse>>(request);

            var resourceUrl = string.Format("{0}v1/uploads/{1}", UPLOAD_BASE_URL, response.Data.First().uploadId);
            return resourceUrl;
        }
    }
}
