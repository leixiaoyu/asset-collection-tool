# Asset Collection Tool

This is a tool to create assets and asset collections in MCP asset collection service. The main use of assets is for image library within DCL.

### Folder structure

The tool expects all resources (csv files, images, xti files, etc.) are organized in a specific way. The tool will ask for the path for root folder, then infers pathes of all resources. As a result, relative location of resources in the root folder is essential in order to get the tool function properly.

```
root folder
|--- assets.csv
|--- collections.csv
|--- assets
     |--- asset-1-preview.jpeg
     |--- asset-1.xti
```

### CSV file format
Sample CSV files can be found in the ```samples``` folder.

##### assets.csv
Here are all required fields in the assets.csv file.
| Name                 | Notes                                                                                                                                                 |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| FileName             | Asset file name                                                                                                                                       |
| Category             | Assigned category of the asset                                                                                                                        |
| CategoryCulture      | Language culture value of the category (e.g. zh-CN). Must follow format listed here: https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx. |
| Keyword              | Assigned keyword value of the asset.                                                                                                                  |
| KeywordCulture       | Language culture value of the keyword (e.g. zh-CN). Must follow format listed here: https://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx   |
| MetadataKey          | Key of one metadata entry for the asset.                                                                                                              |
| MetadataValue        | Value of one metadata entry for the asset                                                                                                             |
| Collection           | Collection name of the asset. Collection name must be presented exactly the same in the collections.csv file.                                         |
| PreviewFileName      | File name of the preview file. It is optional. If it is not provided, the preview file from upload storage will be used.                              |
| DecorationTechnology | Decoration technology of the asset                                                                                                                    |
| WidthInMm            | Width of the asset in milometers                                                                                                                      |
| HeightInMm           | Height of the asset in milometers                                                                                                                     |

##### collections.csv
Here are all required fields in the collections.csv file.
| Name          | Notes                                                                     |
|---------------|---------------------------------------------------------------------------|
| Name          | Name of the collection. Must be the same the name used in assets.csv file |
| MetadataKey   | Key of one metadata entry for the collection                              |
| MetadataValue | Value of one metadata entry for the collection                            |

### Notes
- If choose to preview, formatted JSON request will be printed to console windoes as well as saved to a .json file in root folder.
- When trying to publish collections to asset collection service, the tool will try to reuse an existing JWT. If the operation failed, then it will try to obtain a new token and try a second time.
